from typing import List
import medical_dataclasses as md
from GUI.gui_main import CategoryList
from xml_reader import XmlReader
import os


class MedicalDatabase:
    """
        Class describing the database of Project Hospital.
    """

    def __init__(self, root_dir: str, create_now: bool = True):
        self.root_dir = root_dir

        self.treatments: List[md.Treatment] = []
        self.examinations: List[md.Examination] = []
        self.symptoms: List[md.Symptom] = []
        self.skills: List[md.Skill] = []

        self.db_dict = {}

        if create_now:
            self._create_database()

    def _create_database(self):
        """Uses XmlReader to fill the list with the elements found in self.root_dir directory"""
        reader = XmlReader()
        for file in os.listdir(self.root_dir):
            reader.set_file(os.path.join(self.root_dir, file))

            for elt in reader.find_all():
                if type(elt) is md.Treatment:
                    self.treatments.append(elt)
                elif type(elt) is md.Examination:
                    self.examinations.append(elt)
                elif type(elt) is md.Symptom:
                    self.symptoms.append(elt)
                elif type(elt) is md.Skill:
                    self.skills.append(elt)

        self.db_dict = {
            md.Treatment.NAME: self.treatments,
            md.Examination.NAME: self.examinations,
            md.Symptom.NAME: self.symptoms,
            md.Skill.NAME: self.skills
        }

    def get_element(self, category: str, loc_id: str):
        for elt in self.db_dict[category]:
            if elt.loc_id == loc_id:
                return elt
        raise ValueError(f"Element {loc_id} not found in {category}")


class MedicalDatabaseController:
    def __init__(self, database: MedicalDatabase, category_gui: CategoryList, element_display_gui):
        self.database = database
        self.category_gui = category_gui
        self.element_display_gui = element_display_gui

        self._init_category_ui()

    def _init_category_ui(self):
        for key, value in self.database.db_dict.items():
            self.category_gui.add_category(key)
            self.category_gui.add_items_in_list([x.loc_id for x in value])

        self.category_gui.connect_combo_box(self.update_category_list)
        self.category_gui.connect_list(self.update_element_display)

    def update_category_list(self, name):
        self.category_gui.clear_list()
        self.category_gui.add_items_in_list([x.loc_id for x in self.database.db_dict[name]])

    def update_element_display(self, element_name):
        if element_name == "":
            return

        self.element_display_gui.clear()
        element = self.database.get_element(self.category_gui.get_current_category(), element_name)

        for att in [a for a in dir(element) if (not a.startswith('__')) and a != "NAME"]:
            self.element_display_gui.addItem(f"{att} : {getattr(element, att)}")


if __name__ == '__main__':
    medical_database = MedicalDatabase("database")

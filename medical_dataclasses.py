from dataclasses import dataclass, field
from typing import List, Optional, ClassVar

"""
Dataclasses that describe elements from Project Hospital such as Skill, Examination, Symptom
"""


@dataclass
class Skill:
    """Describes a skill"""
    NAME: ClassVar[str] = "Skill"
    loc_id: str
    loc_description: str
    parent_skill_id: Optional[str]
    icon_index: int
    extra_leveling_percent: Optional[int]


@dataclass
class AnimationSetup:
    """Describes an animation in three steps : in, idle and out."""
    animation_in: str = field(default="")
    animation_idle: str = field(default="")
    animation_out: str = field(default="")


@dataclass
class Procedure:
    """Describes a procedure.
    All the elements are not required depending on what is needed.
    """
    staff_selection_rule: Optional[str] = field(default="")
    required_doctor_qualifications: list = field(default_factory=list)
    required_room_tags: Optional[list] = field(default_factory=list)
    required_equipments: Optional[list] = field(default_factory=list)
    procedure_script: Optional[str] = field(default="")
    animation_setup_lying: Optional[AnimationSetup] = field(default_factory=AnimationSetup)
    animation_setup_sitting: Optional[AnimationSetup] = field(default_factory=AnimationSetup)
    dress_level: Optional[str] = field(default="FULL")
    dress_level_after: Optional[str] = field(default="FULL")
    biohazard: Optional[bool] = field(default=False)
    priority: Optional[int] = field(default=0)


@dataclass
class Treatment:
    """Describes a treatment."""
    NAME: ClassVar[str] = "Treatment"
    loc_id: str = field(default="")
    loc_description: str = field(default="")
    discomfort_level: str = field(default="")
    procedure: Procedure = field(default_factory=Procedure)
    treatment_type: Optional[str] = field(default="")
    icon_index: int = field(default=1)
    duration_hours: int = field(default=0)
    pharmacy_pickup: bool = field(default=False)
    hospitalization_treatment: Optional[str] = field(default="")


@dataclass
class Examination:
    """Describes an examination"""
    NAME: ClassVar[str] = "Examination"
    loc_id: str = field(default="")
    loc_description: str = field(default="")
    discomfort_level: str = field(default="")
    procedure: Procedure = field(default_factory=Procedure)
    examination_type: Optional[str] = field(default="")
    icon_index: int = field(default=1)
    lab_testing_ref: Optional[str] = field(default="")
    duration_hours: int = field(default=0)


@dataclass
class Symptom:
    """Describes a symptom"""
    NAME: ClassVar[str] = "Symptom"
    loc_id: str = field(default="")
    loc_description: str = field(default="")
    discomfort_level: str = field(default="")
    patient_complain: bool = field(default=False)
    hazard: str = field(default="")
    patient_mobility: str = field(default="")
    examinations: List[str] = field(default="")
    treatments: List[str] = field(default="")
    is_main_symp: bool = field(default=False)


@dataclass
class Diagnosis:
    name: str
    description: str
    occurrence: str
    symptoms: list
    examinations: list
    treatments: list
    department: str
    insurance_payement: int

import xml.etree.ElementTree as ET
from medical_dataclasses import Symptom, Treatment, Procedure, Skill, AnimationSetup, Examination
from typing import Optional, Iterable

VALID_TAG = ["GameDBSymptom", "GameDBExamination", "GameDBSkill", "GameDBMedicalCondition", "GameDBRoomType",
             "GameDBTreatment"]


class XmlReader:
    """
    Class reading XML files that describe a Project Hospital's database.
    """

    def __init__(self, path: Optional[str] = None):
        if path is None:
            return
        self.tree = ET.parse(path)  # shall raise an error if the XML file is not readable
        self.root = self.tree.getroot()

        if self.root.tag != "Database":
            raise ValueError('Xml file not compatible')

    def set_file(self, path: str):
        """init the reader with another file"""
        self.__init__(path)

    def find_all(self) -> Iterable:
        """Will run all the iterables to find every element in the file"""
        for elt in self.find_examination():
            yield elt

        for elt in self.find_symptom():
            yield elt

        for elt in self.find_treatments():
            yield elt

        for elt in self.find_skill():
            yield elt

    def find_treatments(self) -> Iterable:
        """
            Finds all the treatments in the file and return corresponding dataclass
        """
        for elt in self.root.findall('GameDBTreatment'):
            treatment = Treatment(elt.attrib["ID"])
            treatment.loc_description = elt.find("AbbreviationLocID").text
            treatment.discomfort_level = elt.find("DiscomfortLevel").text

            proc = elt.find("Procedure")
            if proc is not None:
                treatment.procedure = XmlReader.parse_procedure(proc)
            else:
                print(treatment.loc_id)
                raise ValueError("No procedure found")

            trt_type = elt.find("TreatmentType")
            treatment.treatment_type = None if trt_type is None else trt_type.text

            treatment.icon_index = int(elt.find("IconIndex").text)
            treatment.duration_hours = int(elt.find("DurationHours").text)

            hosp = elt.find("HospitalizationTreatmentRef")
            treatment.hospitalization_treatment = None if hosp is None else hosp.text

            yield treatment

    def find_examination(self) -> Iterable:
        """
            Finds all the examinations in the file and return corresponding dataclass
        """
        for elt in self.root.findall("GameDBExamination"):
            exam = Examination(elt.attrib["ID"])
            exam.loc_description = elt.find("AbbreviationLocID").text
            exam.discomfort_level = elt.find("DiscomfortLevel").text

            exam_type = elt.find("ExaminationType")
            exam.examination_type = None if exam_type is None else exam_type.text
            exam.duration_hours = int(elt.find("DurationHours").text)
            exam.icon_index = int(elt.find("IconIndex").text)

            lab_ref = elt.find("LabTestingExaminationRef")
            exam.lab_testing_ref = None if lab_ref is None else lab_ref.text
            exam.duration_hours = int(elt.find("DurationHours").text)

            # TODO : lab testing equipment

            yield exam

    def find_skill(self) -> Iterable:
        """
            Finds all the skills in the file and return corresponding dataclass
        """
        for elt in self.root.findall('GameDBSkill'):
            loc_id = elt.attrib['ID']
            loc_abb = elt.find("AbbreviationLocID").text
            icon_index = int(elt.find("IconIndex").text)
            extra_lvl_percent = elt.find("ExtraLevelingPercent")

            if extra_lvl_percent is not None:
                extra_lvl_percent = int(extra_lvl_percent.text)

            parent_skill = elt.find("ParentSkill")

            parent_skill_id = None
            if parent_skill is not None:
                parent_skill_id = parent_skill.text

            yield Skill(loc_id, loc_abb, parent_skill_id, icon_index, extra_lvl_percent)

    def find_symptom(self) -> Iterable:
        """
            Finds all the symptoms in the file and return corresponding dataclass
        """
        for elt in self.root.findall("GameDBSymptom"):
            symptom = Symptom(elt.attrib["ID"])
            symptom.loc_description = elt.find("DescriptionLocID").text
            symptom.discomfort_level = elt.find("DiscomfortLevel").text
            symptom.patient_complain = elt.find("PatientComplains").text
            symptom.icon_index = int(elt.find("IconIndex").text)
            symptom.patient_mobility = elt.find("PatientMobility").text
            symptom.hazard = elt.find("Hazard").text
            symptom.is_main_symp = bool(elt.find("IsMainSymptom"))

            symptom.examinations = XmlReader.parse_list(elt.find("Examinations"), "ExaminationRef")

            treatments = elt.find("Treatments")
            symptom.treatments = None if treatments is None else XmlReader.parse_list(treatments, "TreatmentRef")

            yield symptom

    @staticmethod
    def parse_list(element: ET.Element, sub_ref: str) -> list:
        """
            Parse a list of XML elements as:
            <element>
                <sub_ref>A</sub_ref>
                <sub_ref>B</sub_ref>
                <sub_ref>C</sub_ref>
            </element>
            will return: [A, B, C]
            The method raises an error if no element is found.
        :param element: ET.Element - parent element of the list
        :param sub_ref: str - tag of the sub elements
        :return: list - list of items found
        """
        elt_list = []
        for elt in element.findall(sub_ref):
            elt_list.append(elt.text)

        if len(elt_list) == 0:
            raise ValueError("No element found")

        return elt_list

    @staticmethod
    def parse_procedure(procedure: ET.Element) -> Procedure:
        """
            Describes a procedure
        :param procedure: ET.Element - Procedure element
        :return: Procedure dataclass
        """
        p = Procedure()

        staff = procedure.find("StaffSelectionRules")
        p.staff_selection_rule = None if staff is None else staff.text

        proc_script = procedure.find("ProcedureScript")
        p.procedure_script = None if proc_script is None else proc_script.text

        anim_lying = procedure.find("AnimationSetupLying")
        p.animation_setup_lying = None if anim_lying is None else XmlReader.parse_animation(anim_lying)

        anim_sitting = procedure.find("AnimationSetupSitting")
        p.animation_setup_sitting = None if anim_sitting is None else XmlReader.parse_animation(anim_sitting)

        dress_lvl = procedure.find("DressLevel")
        p.dress_level = None if dress_lvl is None else dress_lvl.text

        dress_lvl = procedure.find("DressLevelAfter")
        p.dress_level_after = None if dress_lvl is None else dress_lvl.text

        required_skills = procedure.find("RequiredDoctorQualificationList")
        if required_skills is None:
            required_skills = procedure.find("RequiredSkillsToPrescribe")

        p.required_doctor_qualifications = XmlReader.parse_list(required_skills, "SkillRef")

        room_tags = procedure.find("RequiredRoomTags")
        p.required_room_tags = None if room_tags is None else XmlReader.parse_list(room_tags, "Tag")

        equipments = procedure.find("RequiredEquipmentList")
        p.required_equipments = None if equipments is None else XmlReader.parse_equipment_list(equipments)

        bio = procedure.find("Biohazard")
        p.biohazard = None if bio is None else bio.text

        priority = procedure.find("Priority")
        p.priority = None if priority is None else int(priority.text)

        return p

    @staticmethod
    def parse_animation(anim: ET.Element) -> AnimationSetup:
        """
            Parse animation from XML to dataclass
        :param anim: ET.Element - animation element
        :return: Animation dataclass
        """
        animation = AnimationSetup()
        animation.animation_in = anim.find("AnimationNameIn").text
        animation.animation_idle = anim.find("AnimationNameIdle").text
        animation.animation_out = anim.find("AnimationNameOut").text

        return animation

    @staticmethod
    def parse_equipment_list(element: ET.Element):
        """
            Parse equipment list.
        :param element: ET.Element
        :return: list of equipments
        """
        if element.tag != "RequiredEquipmentList":
            raise ValueError(element.tag + " is not an equipment's list")

        elt_list = []
        for elt in element.findall("RequiredEquipment"):
            elt_list.append(elt.find("Tag").text)

        if len(elt_list) == 0:
            raise ValueError("No element found")

        return elt_list


def main():
    reader = XmlReader("database/Treatments.xml")
    for e in reader.find_treatments():
        print(e)
        # break


if __name__ == '__main__':
    main()

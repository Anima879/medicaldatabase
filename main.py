from GUI.gui_main import MainGui
from medical_database import MedicalDatabase, MedicalDatabaseController
import os

ROOT = "database"
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


def main():
    mdb = MedicalDatabase(ROOT)
    main_gui = MainGui(os.path.join(ROOT_DIR, "ui_files"))

    mdb_controller = MedicalDatabaseController(mdb, main_gui.main_categories, main_gui.element_list)

    main_gui.execute_app()


if __name__ == '__main__':
    main()

from PySide6.QtCore import QFile, QIODevice
from PySide6.QtWidgets import QApplication
from PySide6.QtUiTools import QUiLoader
import sys
import os


def load_ui_file(ui_file_name: str, parent=None):
    ui_file = QFile(ui_file_name)

    if not ui_file.open(QIODevice.ReadOnly):
        raise IOError("Cannot open {}: {}".format(ui_file_name, ui_file.errorString()))

    loader = QUiLoader()
    widget = loader.load(ui_file, parent)
    ui_file.close()

    if not widget:
        print(loader.errorString())
        sys.exit(-1)

    return widget


class CategoryList:
    def __init__(self, ui_file_path: str):
        self.widget = load_ui_file(ui_file_path)

    def add_category(self, name):
        self.widget.categoryComboBox.addItem(name)

    def add_item_in_list(self, item: str):
        self.widget.categoryList.addItem(str(item))

    def add_items_in_list(self, items: list):
        for item in items:
            self.add_item_in_list(item)

    def clear_list(self):
        self.widget.categoryList.clear()

    def connect_combo_box(self, func):
        self.widget.categoryComboBox.currentTextChanged.connect(func)

    def connect_list(self, func):
        self.widget.categoryList.currentTextChanged.connect(func)

    def get_current_category(self) -> str:
        return self.widget.categoryComboBox.currentText()


class ElementList:
    def __init__(self):
        pass


class MainGui:
    def __init__(self, ui_files_dir_path):
        self.app = QApplication(sys.argv)

        self.main_window = load_ui_file(os.path.join(ui_files_dir_path, "main_window.ui"))
        self.main_categories = CategoryList(os.path.join(ui_files_dir_path, "category_widget.ui"))

        self.main_window.category_layout.addWidget(self.main_categories.widget)
        self.element_list = self.main_window.element_list

    def execute_app(self):
        self.main_window.show()
        sys.exit(self.app.exec())


if __name__ == '__main__':
    main_gui = MainGui("../ui_files")
